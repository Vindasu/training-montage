from common.json import ModelEncoder

from .models import BubbleSort

class TestModelEncoder(ModelEncoder):
    model = BubbleSort
    properties = [
        "unsorted_list",
        "iterations",
        "sorted_list"
        ]
