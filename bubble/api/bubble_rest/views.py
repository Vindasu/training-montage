from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import BubbleSort
from .acls import get_photo
from .encoders import TestModelEncoder
from .calculations import bubble_sort

def api_bubblesort(request, unsorted_list):
    content = json.loads(request.body)
    bubble_sort(unsorted_list)
    return JsonResponse(
        content,
        encoder=TestModelEncoder,
        safe=False
    )
    

# @require_http_methods(["DELETE", "GET", "PUT"])
# def show_testmodel(request, id):
#     if request.method == "GET":
#         testmodel = TestModel.objects.get(id=id)
#         return JsonResponse(
#             testmodel,
#             encoder=TestModelEncoder,
#             safe=False
#         )
#     elif request.method == "DELETE":
#         count, _ = TestModel.objects.filter(id=id).delete()
#         return JsonResponse({"deleted": count > 0})
#     else:
#         content = json.loads(request.body)
#         more here
