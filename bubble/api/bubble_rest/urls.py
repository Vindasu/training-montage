from django.urls import path

from .views import api_bubblesort

urlpatterns = [
    path("bubblesort/", api_bubblesort, name="api_bubblesort")
 ]
