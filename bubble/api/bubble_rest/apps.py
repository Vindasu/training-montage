from django.apps import AppConfig


class bubbleRestConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'bubble_rest'
