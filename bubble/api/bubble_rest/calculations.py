from django.db import models


def bubble_sort(unsorted_list):
    n = len(unsorted_list)
    for i in range(n - 1):
        for j in range(0, n - i - 1):
            if unsorted_list[j] > unsorted_list[j + 1]:
                unsorted_list[j], unsorted_list[j + 1] = unsorted_list[j + 1], unsorted_list[j]
 
# BubbleSort([64, 34, 25, 12, 22, 11, 90])
 
# print("Sorted array is:")
# for i in range(len(arr)):
#     print("% d" % arr[i], end=" ")
