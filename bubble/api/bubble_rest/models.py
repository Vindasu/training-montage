from django.db import models
from django.urls import reverse


class BubbleSort(models.Model):
    unsorted_list = models.TextField(max_length=1000)
    iterations = models.PositiveSmallIntegerField()
    sorted_list = models.TextField(max_length=1000)

    def __str__(self):
        return self.name
