import './App.css';
import './index.css'
import React from 'react';

function About() {
    // const modalRef = useRef()
    
    // const showModal = () => {
    //     const modalEle = modalRef.current
    //     const bsModal = new Modal(modalEle, {
    //         backdrop: 'static',
    //         keyboard: false
    //     })
    //     bsModal.show()
    // }
    
    // const hideModal = () => {
    //     const modalEle = modalRef.current
    //     const bsModal= bootstrap.Modal.getInstance(modalEle)
    //     bsModal.hide()
    // }
    

    return (

    <>
    <div className="about-section">
        <h1>About Us</h1>
        <p>We are a team of software engineers that wanted to practice algorithms.</p>
        <p>In order to do this, Alex Adams created an algo prep application for his squad.</p>
    </div>
        <div/> 
        {/* <script>
        $('#modal_id').modal('show').on('shown.bs.modal', function() {
        $('input_element_id').focus()
        });
        </script> */}
        <div className="row row-cols-5 row-cols-md-9 g-9 px-10">

                <div className="col">
                    <div className="card">
                    <img src="https://i.imgur.com/hPoszjY.png" alt="Alex" style={{width:" 100%"}}/>
                    <div className="card-body">
                            <h5>Alex Adams</h5>
                            <p className="Card-Text">Full-Stack Software Engineer</p>
                            <p>Alex enjoys lorem ipsum text. Lorem ipsum dolor sit amet, consectetur. </p>
                            <p>lorem@ipsum.com</p>
                            {/* <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">Contact Info</button>
                            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                        555-555-5555
                                        donotcontact@goaway.com
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                    </div>
                                </div>
                                </div> */}
                    </div>
                    </div>
                    </div>
                    <div className="col">
                    <div className="card">
                    <img src="https://i.imgur.com/E2LJilX.png" alt="Alex #2" style={{width:" 100%"}}/>
                    <div className="card-body">
                            <h5 className="Card-Title">Alex Anley</h5>
                            <p className="card-text">Back-End Software Engineer</p>
                            <p>Alex holds the world record for most stress accumulated in a 30 second period.</p>
                            <p>alex.anley@dontemailme.com</p>
                    </div>
                            {/* <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">Contact Info</button>
                            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                        555-555-5555
                                        donotcontact@goaway.com
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                    </div>
                                </div>
                                </div> */}

        </div>
                </div>
                    <div className="col">
                    <div className="card">
                    <img src="https://i.imgur.com/LaAGCRJ.png" alt="Ethan" style={{width:" 100%"}}/>
                    <div className="card-body">
                            <h5>Ethan Beiber</h5>
                            <p className="Card-Text">Back-End Software Engineer</p>
                            <p>Ethan enjoys long walks on the beach with his cousin Justin Beiber.</p>
                            <p>beiberfan@dolphingang.com</p>
                            {/* <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">Contact Info</button>
                            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                        555-555-5555
                                        donotcontact@goaway.com
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                    </div>
                                </div>
                                </div> */}
                        </div>
                        </div>
                        </div>
                    <div className="col">
                    <div className="card">
                    <img src="https://i.imgur.com/lT9t2nq.png" alt="Corey" style={{width:" 100%"}}/>
                        <div className="card-body">
                            <h5>Corey Schanz</h5>
                            <p className="Card-Text">Back-End Software Engineer</p>
                            <p>Corey lives with his chef dad at the white house.</p>
                            <p>coreyinthehouse@guv.com</p>
                            {/* <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">Contact Info</button>
                            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                        555-555-5555
                                        donotcontact@goaway.com
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                    </div>
                                </div>
                                </div> */}
                        </div>
                        </div>
            </div>
                    
            </div>
        

        </>

);
}

export default About;
