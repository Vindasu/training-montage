function Bubble() {
    return (
        <div className="Container">
            <h1>Welcome to Bubble Sort!</h1>
            <p>
                Bubble Sort is a simple sorting algorithm that works by continuously swapping items in a list that are next to each other (if they are not in the correct order). While this is a simple algorithm to implement, it is not ideal for larger quanities of data as the average and worst case time complexity is higher than other sorting algorithms.
            </p>
            <h3>
                How to Use Bubble Sort:
            </h3>
            <p>
                <button>Sort my list!</button>
            </p>
        </div>
    )
}

export default Bubble
