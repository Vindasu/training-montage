import './App.css';
import React from 'react';
import { Routes, Route } from 'react-router-dom';
// import BackGround from './Background';
import MainPage from './MainPage';
import About from './About';
import Nav from './Nav';
import Merge from './Merge';
import Quick from './Quick';
import Bubble from './Bubble';

function App() {
  return (
    <div className="font-link">
    {/* <BackGround/> */}
    <Nav/>
      <Routes>
        <Route path="/" element={<MainPage />} />
        <Route path="/about" element={<About />} />
        <Route path="/quick" element={<Quick />} />
        <Route path="/merge" element={<Merge />} />
        <Route path="/bubble" element={<Bubble />} />
      </Routes>
    
    </div>
  );
}

export default App;
