import './App.css';
import { NavLink } from 'react-router-dom';
import React from 'react';

function MainPage(){
    return(

<div className="App">
    <header className="App-header">
        <img src="https://i.imgur.com/rboL7Nk.png" className="App-logo" alt="logo" />
        <p>
        Welcome to the internet, squad! This is where your algorithm journey will begin...
        </p>
        <a
        className="App-link"
        href="https://reactjs.org"
        target="_blank"
        rel="noopener noreferrer"
        >
        Click to expand your mind
        </a>
    </header>
    </div>
    )
}
export default MainPage;