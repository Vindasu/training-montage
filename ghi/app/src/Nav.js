import { NavLink, Link } from 'react-router-dom';
import React, { useEffect, useState, useContext} from 'react';
import {CgProfile} from 'react-icons/cg'

function Nav() {
      return(
        <>
            <NavLink className="navbar-brand" aria-current="page" to="/">Home</NavLink>
            <NavLink className="navbar-brand" aria-current="page" to="/about">About Us</NavLink>
            <NavLink className="navbar-brand" aria-current="page" to="/quick">Quick Sort</NavLink>
            <NavLink className="navbar-brand" aria-current="page" to="/merge">Merge Sort</NavLink>
            <NavLink className="navbar-brand" aria-current="page" to="/bubble">Bubble Sort</NavLink>
          <div className='d-flex justify-content-between text-white align-items-center'>
  
            </div>
        </>  
      )
    }  

export default Nav;
