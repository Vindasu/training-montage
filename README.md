## WELCOME to the Training Montage

# Members
- Alex Adams
- Alex Anley
- Ethan Beber
- Corey Schanz

## Intro

This is a Django/React application designed to provide experience implementing common sorting algorithms in a realistic environment. 

The goal is simple: 
Implement your algorithm functionally in a django project.
Use your front-end knowledge to create an interface that accepts an unsorted list, and after a button press: shows how many iterations occurred, and returns a sorted list.

Creativity is encouraged, but focus on getting this live with a functioning algorithm.

## Setup

docker volume create squad-data
docker-compose build
docker-compose up

Corey is assigned to Merge Sort
Alex is assigned to Bubble Sort
Ethan is assigned to Quick Sort