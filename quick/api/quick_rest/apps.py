from django.apps import AppConfig


class quickRestConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'quick_rest'
